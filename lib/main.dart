import 'package:flutter/material.dart';

void main() => runApp(HelloFlutterApp());
// class HelloFlutteerApp extends StatelessWidget{
//   @override
//   Widget build(BuildContext context){
//     return MaterialApp(
//       debugShowCheckedModeBanner:(false),
//        home: Scaffold(
//          appBar: AppBar(
//            //Scaffold Widget
//            title: Text("Hello Flutter"),
//            leading: Icon(Icons.home),
//            actions: <Widget>[
//              IconButton(
//                  onPressed: (){},//function
//                  icon: Icon(Icons.refresh))
//            ],
//          ),
//          body:Center(
//            child:Text(
//                "Hello Flutter !",
//            style: TextStyle(fontSize: 24),
//            ),
//          ),
//        ),
//     );
//   }
// }

class HelloFlutterApp extends StatefulWidget{
  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}
String englishGreeting = "Hello Flutter";
String spanishGreeting = "Hola Flutter";
String japaneseGreeting = "こんにちは Futter";
String chineseGreeting = "你好 Flutter";
class _MyStatefulWidgetState extends State <HelloFlutterApp>{
  String displayText = englishGreeting;
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      debugShowCheckedModeBanner:(false),
       home: Scaffold(
         appBar: AppBar(
           //Scaffold Widget
           title: Text("Hello Flutter"),
           leading: Icon(Icons.home),
           actions: <Widget>[
             IconButton(
                 onPressed: (){
                   setState(() {
                     displayText = displayText == englishGreeting?
                     spanishGreeting : englishGreeting;
                   });
                 },//function
                 icon: Icon(Icons.refresh)),
             IconButton(
                 onPressed: (){
                   setState(() {
                     displayText = displayText == englishGreeting?
                     japaneseGreeting : englishGreeting;
                   });
                 },//function
                 icon: Icon(Icons.translate)),
             IconButton(
                 onPressed: (){
                   setState(() {
                     displayText = displayText == englishGreeting?
                     chineseGreeting : englishGreeting;
                   });
                 },//function
                 icon: Icon(Icons.language_rounded)),
           ],

         ),
         body:Center(
           child:Text(
               displayText,
           style: TextStyle(fontSize: 24),
           ),
         ),
       ),
    );
  }
}

